package jp.junoe.confluence.plugins.ditamacro.xhtml;

import java.util.Map;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.MacroExecutionException;

/**
 * This very simple macro shows you the very basic use-case of displaying
 * *something* on the Confluence page where it is used. Use this example macro
 * to toy around, and then quickly move on to the next example - this macro
 * doesn't really show you all the fun stuff you can do with Confluence.
 */
public abstract class DitaBlockMacro extends DitaMacro {

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
        String tagName = getTagName();
        return "<div data-ditatag=\""+ tagName +"\" class=\"dita-tag dita-block-tag dita-tag-" + tagName + "\" " + getParameterString(parameters) + "/>" + body + "</div>";
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.RICH_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
