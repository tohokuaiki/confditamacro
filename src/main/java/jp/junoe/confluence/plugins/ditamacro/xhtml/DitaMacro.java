package jp.junoe.confluence.plugins.ditamacro.xhtml;

import java.util.Map;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import org.apache.commons.lang3.StringUtils;

/**
 * This very simple macro shows you the very basic use-case of displaying
 * *something* on the Confluence page where it is used. Use this example macro
 * to toy around, and then quickly move on to the next example - this macro
 * doesn't really show you all the fun stuff you can do with Confluence.
 */
public abstract class DitaMacro implements Macro {

    /**
     *
     * @param parameters
     * @param body
     * @param context
     * @return
     * @throws MacroExecutionException
     */
    @Override
    public abstract String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException;

    /**
     *
     * @return
     */
    @Override
    public abstract BodyType getBodyType();

    /**
     *
     * @return
     */
    @Override
    public abstract OutputType getOutputType();

    protected String getParameterString(Map<String, String> parameters) {
        String params = "";
        String param_value;
        for (String param_key : parameters.keySet()) {
            if (param_key.equals(com.atlassian.renderer.v2.macro.Macro.RAW_PARAMS_KEY)){
                continue;
            }
            param_value = parameters.get(param_key);
            if (!StringUtils.isEmpty(param_value)) {
                if (params.length() > 0) {
                    params += " ";
                }
                params += "data-ditaparameter-" + param_key + "=\"" + param_value + "\"";
            }
        }

        return params;
    }

    /**
     * 現在のクラス名からTagNameを返却する
     *
     * @return
     */
    protected String getTagName() {
        String tagName;
        tagName = getClass().getSimpleName().toLowerCase().substring(4); // "4" means Dita
        tagName = tagName.substring(0, tagName.length() - 5); // "5" means Macro

        return tagName;
    }
}
